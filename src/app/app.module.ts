import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; 

import { ApiService } from './api.service'
import { RegisterService } from './register/register.service'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HomepageComponent } from './homepage/homepage.component';
import { QuotationComponent } from './quotation/quotation.component';
import { BookingComponent } from './booking/booking.component';
import { PostComponent } from './post/post.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    BookingComponent,
    QuotationComponent,
    HomepageComponent,
    PostComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [ApiService , 
              RegisterService
             ],
  bootstrap: [AppComponent]
})
export class AppModule { }
