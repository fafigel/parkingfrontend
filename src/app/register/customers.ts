export class Customers{

    public licenseNumber:string;
    public firstName:string;
    public surname:string;
    public gender:string;
    public created_at:Date;
    public updated_at:Date;

}