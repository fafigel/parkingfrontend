import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { RegisterService } from '../register/register.service';
import { Customers } from './customers';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers:[RegisterService, Customers]
})
export class RegisterComponent /*implements OnInit*/ {

  submitted: boolean = false;
  public customers : Customers = new Customers();
  

  constructor(
    public registerService: RegisterService , public acRoute : ActivatedRoute
    ){}

    /*createRegister(register){
      this.submitted = true;
      this.registerService.post(register)
          .subscribe(
            data => {return true},
            
          )
    }*/
    
    ngOnInit() {

      this.submitted = true;
      this.acRoute.params.subscribe((data : any)=>{
      console.log(data.id);
      if(data && data.id){
          this.registerService.get("customers/"+data.id).subscribe((data : Customers)=>{
          this.customers = data;
          });
      }
      else
      {
          this.customers = new Customers();
      }
      })
  }

    public onSubmit(){
    console.log("Regerstering a customer: "
    + this.customers.licenseNumber
    + this.customers.firstName 
    + this.customers.surname 
    + this.customers.gender
    );
    if(this.customers.licenseNumber){
    this.registerService.update(""+this.customers.licenseNumber,this.customers).subscribe((r)=>{
        console.log(r);
        alert("Customer Registered !");
    })
    }
    if(this.customers.firstName ){
      this.registerService.update(""+this.customers.firstName ,this.customers).subscribe((r)=>{
          console.log(r);
          alert("Customer Registered !");
      })
    }
    else
    this.registerService.post("",this.customers).subscribe((r)=>{
    console.log(r);
    this.customers = new Customers();
    alert("Customer Registered !");

    });
}


/*public pictures: Array<Picture>;

  ngOnInit() {

  		 this.apiService.get("pictures").subscribe((data : Picture[])=>{
    	 console.log(data);
    	 this.pictures = data;
    	 });
  }*/

}

